let names = document.querySelector('#name');
let phones = document.querySelector('#phone');
let emails = document.querySelector('#email');

let error = document.createTextNode('Invalide!');//NAME
let errorUn = document.createTextNode('Invalide!');//EMAIL
let errorDeux = document.createTextNode('Invalide!');//PHONE


document.querySelector('#signup-form').addEventListener('focusout', verifier);
document.querySelector('#signup-form').addEventListener('focusout', valideEmail);
document.querySelector('#signup-form').addEventListener('focusout', validePhone);


//INPUT MAX CHARACTERS
function inputMaxName() {
    names.maxLength = 50;
}
inputMaxName();

function inputMaxTel() {
    phones.maxLength = 14;
}
inputMaxTel();

//VERIFICATION
function verifier(e) {
    e.preventDefault();

    if (names.value === "") {
        names.style.border = "2px solid red";
        names.parentElement.style.color = 'red';
        names.parentElement.append(error);
    }
    else {
        names.style.border = "2px solid green";
        error.remove();
    }

    if (emails.value === "") {
        emails.style.border = "2px solid red";
        emails.parentElement.style.color = 'red';
        emails.parentElement.append(errorUn);
    }
    else {
        emails.style.border = "2px solid green";
        errorUn.remove();
    }

    if (phones.value === "") {
        phones.style.border = "2px solid red";
        phones.parentElement.style.color = 'red';
        phones.parentElement.append(errorDeux);
    }
    else {
        phones.style.border = "2px solid green";
        errorDeux.remove();
    }

}

document.querySelector('#signup-form #submit').addEventListener("click", verifier);

//VERIF TYPE CHARACTER
function checkEmail(emails) {

    let re = /^[\w-.]+@([\w-]+.)+[\w-]{2,4}$/;
    return re.test(emails);
}

function valideEmail() {
    if (checkEmail(emails.value)) {
        alert('Adresse e-mail valide');
        emails.style.border = "2px solid green";
    } else {
        alert('Adresse e-mail non valide');
        emails.style.border = "2px solid red";
    }
    return false;
}

document.querySelector('#signup-form #submit').addEventListener("click", valideEmail);

function checkPhone(phones) {

    let re = /^0[6-7]([-. ]?[0-9]{2}){4}$/;
    return re.test(phones);
}

function validePhone() {

    if (checkPhone(phones.value)) {
        alert('Numero valide');
        phones.style.border = "2px solid green";
    } else {
        alert('Numero non valide');
        phones.style.border = "2px solid red";
    }
    return false;
}

document.querySelector('#signup-form #submit').addEventListener("click", validePhone);









